var postId=0;
var postBodyElement=null;
var userId=0;
$('.post').find('.interaction').find('a.edit').on('click',function(event){
	event.preventDefault();
	postBodyElement=$(this).parent().parent().find('.post-body');
	var postBody=postBodyElement.text();
	postId=$(this).parent().parent().data('postid');
	userId=$(this).data('user_id');
	$('#post-body').val(postBody);
	$('#edit-modal').modal();
});

$('#modal-save').on('click',function(){
	$.ajax({
		method:'POST',
		url:urlEdit,
		data:{body:$('#post-body').val(),postId:postId,_token:token,userId:userId}
	}).done(function(msg){
		$(postBodyElement).text(msg['new_body']);
		$("#edit-modal").modal('hide');
	}).fail(function(error){
		console.log(error);
	});
});

$('.like').on('click', function(event) {
    event.preventDefault();
    postId = event.target.parentNode.parentNode.dataset['postid'];
    var Like = $(this).data('value');
    console.log(postId+"<br>");
    console.log(Like+"<br>");
    var element=$(this);
    $.ajax({
        method: 'POST',
        url: urlLike,
        data: {Like: Like, postId: postId, _token: token}
    })
        .done(function() {
        	//console.log("like:"+Like);
        	if(Like=="1"){
        		element.text('You like this post');
        		element.next().text('Dislike');
        		element.data("value", "remove1");
        	}
        	if(Like=="0"){
        		element.text('You don\'t like this post');
        		element.prev().text('Like');
        		element.data("value", "remove0");
        	}
        	if(Like=="remove1"){
        		element.text('Like');
        		element.next().text('Dislike');
        		element.data("value", "1");
        	}
        	if(Like=="remove0"){
        		element.text('Dislike');
        		element.prev().text('Like');
        		element.data("value", "0");
        	}
        });
});