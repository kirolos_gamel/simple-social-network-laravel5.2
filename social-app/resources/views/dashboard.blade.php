@extends('layouts.master')

@section('title')
Social Network-Dashboard
@endsection

@section('content')
@include('includes.message')
<section class="row new-post">
	<div class="col-md-6 col-md-offset-3">
            <header><h3>What do you have to say?</h3></header>
            <form action="{{ action('PostController@postCreate') }}" method="post">
            	<div class="form-group">
                    <textarea class="form-control" name="body" id="new-post" rows="5" placeholder="Your Post"></textarea>
                </div>
                {{csrf_field()}}
                <button type="submit" class="btn btn-primary">Create Post</button>
            </form>
    </div>        
</section>
<section class="row posts">
    <div class="col-md-6 col-md-offset-3">
        <header><h3>What other people say...</h3></header>
        @foreach($posts as $post)
        <article class="post" data-postid="{{ $post['post']->id }}">
        	<p class="post-body">{{$post['post']->body}}</p>
        	<div class="info">
        		Posted by {{$post['user'][0]->name}} on {{$post['post']->created_at}}
        	</div>
        	<div class="interaction">
        		<a href="#" class="like" data-value="1">
	        		@if($post['like'][0])
	        		   @if($post['like'][0]->like==0)
	        		      Like
	        		   @elseif($post['like'][0]->like==1)
	        		      You like this post  
	        		   @endif
	        		@else
	        		   Like   
	        		@endif
        		</a> | 
        		<a href="#" class="like" data-value="0">
	        		@if($post['like'][0])
	        		   @if($post['like'][0]->like==0)
	        		      You don\'t like this post
	        		   @elseif($post['like'][0]->like==1)
	        		     Dislike 
	        		   @endif
	        		@else
	        		   Dislike   
	        		@endif
        		</a> | 
        		@if(Auth::user()->id==$post['post']->user_id)
	        		<a href="#" class="edit" data-user_id="{{$post['user'][0]->id}}">Edit</a> |
	        		<a href="{{route('post.delete',['id'=>$post['post']->id,'user_id'=>$post['user'][0]->id]) }}" class="delete">Delete</a> |
	        	@endif
	        		
        	</div>
        </article>
        @endforeach
    </div>
</section>
<div class="modal fade" tabindex="-1" role="dialog" id="edit-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Post</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="post-body">Edit the Post</label>
                        <textarea class="form-control" name="post-body" id="post-body" rows="5"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modal-save">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    var token = '{{ Session::token() }}';
    var urlEdit = '{{ route('post.edit') }}';
    var urlLike = '{{ route('post.like') }}';
</script>            
@endsection


