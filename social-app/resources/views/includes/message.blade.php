@if(count($errors) > 0)
    <div class="row">
    	<div class="col-md-4 col-md-offset-4 error">
    		<ul>
    			@foreach($errors->all() as $error)
    			   <li>{{$error}}</li>
    			@endforeach   
    		</ul>
    	</div>
    </div>
@endif

@if(!empty($error))
    <div class="row">
    	<div class="col-md-4 col-md-offset-4 error">
    	     {{$error}}
    	</div>
    </div>
@endif

@if(Session::has('message'))
    <div class="row">
    	<div class="col-md-4 col-md-offset-4 success">
    		{{Session::get('message')}}
    	</div>
    </div>
@endif 

@if(Session::has('success'))
    <div class="row">
    	<div class="col-md-4 col-md-offset-4 success">
    		{{Session::get('success')}}
    	</div>
    </div>
@endif

@if(Session::has('error'))
    <div class="row">
    	<div class="col-md-4 col-md-offset-4 success">
    		{{Session::get('error')}}
    	</div>
    </div>
@endif        

@if(!empty($success))
    <div class="row">
    	<div class="col-md-4 col-md-offset-4 success">
    		{{$success}}
    	</div>
    </div>
@endif  