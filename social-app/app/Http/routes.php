<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::auth();


Route::get('/',['middleware'=>'auth','uses'=>'PostController@getDashboard'])->name('index');

Route::post('/createpost',['uses'=>'PostController@postCreate','as'=>'post.create']);

Route::get('/deletepost/{id}/{user_id}',['uses'=>'PostController@deletePost','as'=>'post.delete']);

Route::post('/editpost',['uses'=>'PostController@editPost','as'=>'post.edit']);

Route::post('/likepost',['uses'=>'PostController@likePost','as'=>'post.like']);


Route::get('/account',['uses'=>'UserController@getAccount','as'=>'account']);

Route::post('/upateaccount', ['uses' => 'UserController@saveAccount','as' => 'account.save']);

Route::get('/userimage/{filename}', [
	    'uses' => 'UserController@getUserimage',
	    'as' => 'account.image'
	]);