<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use DB;
use Storage;
use File;
use Illuminate\Http\Response;

class UserController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }
	
	public function getAccount(){
		 $user = Auth::user();
		return view('account',['user'=>$user]);	
    }
	
	public function getUserimage($filename){
		$file = Storage::disk('local')->get($filename);
        return new Response($file, 200);
	}
	
	public function saveAccount(Request $request){
		$this->validate($request, [
           'name' => 'required|max:120'
        ]);

        $user = Auth::user();
        $old_name = $user->name;
        $user->name = $request['name'];
        $user->update();
        $file = $request->file('image');
        $filename = $request['name'] . '-' . $user->id . '.jpg';
        $old_filename = $old_name . '-' . $user->id . '.jpg';
        $update = false;
        if (Storage::disk('local')->has($old_filename)) {
            $old_file = Storage::disk('local')->get($old_filename);
            Storage::disk('local')->put($filename, $old_file);
            $update = true;
        }
        if ($file) {
            Storage::disk('local')->put($filename, File::get($file));
        }
        if ($update && $old_filename !== $filename) {
            Storage::delete($old_filename);
        }
        return redirect()->route('account');
		
	}
}
