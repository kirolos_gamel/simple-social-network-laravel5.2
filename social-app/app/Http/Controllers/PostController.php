<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\services;

use Illuminate\Support\Facades\Auth;
use DB;

class PostController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }
	
	public function getDashboard(){
		$service=new services();
		$posts=$service->getDashboard();
		return view('dashboard',['posts'=>$posts]);
	}
	
	public function postCreate(Request $request){
		$this->validate($request,["body"=>"required|max:1000"]);
		$body=$request['body'];
		$user=Auth::user();
		$services=new services();
		$message="There Is Error.";
		if($services->save_post($body,$user->id)){
			$message="Post successfull Added.";
		}
		return redirect('/')->with(['message'=>$message]);
	}
	
	public function editPost(Request $request){
		$this->validate($request,["body"=>"required"]);
		if(Auth::user()->id!=$request['userId']){
			return redirect()->back();
		}
	    $service=new services();
		if($service->editPost($request['postId'],$request['body'])){
	        return response()->json(['new_body'=>$request['body']],200);
		}
		else{
			return response()->json(['error'=>"fail edit"]);
		}
	}
	
	public function deletePost($id,$user_id){
		if(Auth::user()->id!=$user_id){
			return redirect()->back();
		}
		$service=new services();
		if($service->deletePost($id)){
			return redirect()->route('index')->with(['message'=>'Successfully Deleted.']);
		}
		else{
			return redirect()->route('index')->with(['error'=>'Fail Deleted.']);
		}
	}
	
	public function likePost(Request $request){
		$this->validate($request,['Like'=>'required',"postId"=>"required"]);
		$post_id = $request['postId'];
        $Like = $request['Like'];
        $user = Auth::user();
		if($Like=="remove1" or $Like=="remove0"){
			$delete=DB::table('likes')->where([['user_id',$user->id],["post_id",$post_id]])->delete();
			return null;
		}
		else{
			$like=DB::table('likes')->where([['user_id',$user->id],["post_id",$post_id]])->get();
			if($like){
				$update=DB::table('likes')->where([['post_id',$post_id],['user_id',$user->id]])->update(['like'=>$Like]);
				return null;
			}
			else{
				$like=DB::table('likes')->insert(['post_id'=>$post_id,'like'=>$Like,'created_at'=>date("Y-m-d h:i:sa"),'updated_at'=>date("Y-m-d h:i:sa"),'user_id'=>$user->id]);
				return null;
			}
		}
		
	}
	
}
