<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Auth;

class services extends Model
{
    public function save_post($body,$id){
    	return DB::table('posts')->insert(['body'=>$body,'created_at'=>date("Y-m-d h:i:sa"),'updated_at'=>date("Y-m-d h:i:sa"),'user_id'=>$id]);
    }
	
	public function getDashboard(){
		
		$posts=DB::table('posts')->orderBy('created_at','desc')->get();
		//$likes=DB::table('likes')->where('user_id',Auth::user()->id)->get();
		$final_result=array();
		
		foreach ($posts as $post) {
			$user=DB::table('users')->select('id','name')->where('id','=',$post->user_id)->get();
			$like=DB::table('likes')->where([['post_id',$post->id],['user_id',Auth::user()->id]])->get();
			if($like){
				$row=array("post"=>$post,"user"=>$user,"like"=>$like);
			}
			else{
				$row=array("post"=>$post,"user"=>$user,"like"=>null);
			}
			array_push($final_result,$row);
	    }
		return $final_result;
	}
	
	public function deletePost($id){
		return DB::table('posts')->where('id','=',$id)->delete();
	}
	
	public function editPost($id,$body){
		return DB::table('posts')->where('id','=',$id)->update(['body'=>$body]);
	}
	
	public function likePost(){
		
	}
}
